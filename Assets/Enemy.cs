﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Character character;
    private void Update()
    {
        if (Vector3.Distance(transform.position, character.transform.position) > 5)
        {
            agent.isStopped = false;
            agent.SetDestination(character.transform.position);
        }
        else
        {
            agent.isStopped = true;
        }
    }

    public void ReceiveDamage(int damage)
    {
        print($"received {damage} damage");
    }
}
