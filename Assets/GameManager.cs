﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject settingsPanel; 
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            if (settingsPanel.activeSelf)
            {
                settingsPanel.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                settingsPanel.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}
