﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    [SerializeField] private Text infoText;
    [SerializeField] private GameObject yesButton;
    [SerializeField] private GameObject noButton;
    [SerializeField] private GameObject proceedButton;

    private void Start()
    {
        NewInfoPanel().WithText("hello").WithYesNoButton();
    }

    public InfoPanel NewInfoPanel()
    {
        infoText.text = "";
        yesButton.SetActive(false);
        noButton.SetActive(false);
        proceedButton.SetActive(false);
        return this;
    }

    public InfoPanel WithText(string txt)
    {
        infoText.text = txt;
        return this;
    }

    public InfoPanel WithYesNoButton()
    {
        yesButton.SetActive(true);
        noButton.SetActive(true);
        return this;
    }
    
}
