﻿using System;
using UnityEngine;

namespace _Scripts
{
    public class AutoDestroy : MonoBehaviour
    {
        [SerializeField] private float timer;


        private void Start()
        {
            Destroy(gameObject, timer);
        }
    }
}
