﻿using System;
using UnityEngine;

namespace _Scripts
{
	public class CameraController : MonoBehaviour
	{
		[SerializeField] private Transform character;
		[SerializeField] private float sensitivity;


		private float _currentVertical = 0f;

		private void Start()
		{
			Cursor.lockState = CursorLockMode.Locked;
		}

		private void Update()
		{
			var mouseX = Input.GetAxis("Mouse X");
			var mouseY = Input.GetAxis("Mouse Y");

			_currentVertical -= mouseY * sensitivity * Time.deltaTime;
			_currentVertical = Mathf.Clamp(_currentVertical, -90f, 90f);

			transform.localRotation = Quaternion.Euler(_currentVertical, 0f, 0f);
			character.Rotate(0f, mouseX * sensitivity * Time.deltaTime, 0f);
		}
	}
}
