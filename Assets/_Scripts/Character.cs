﻿using System;
using _Scripts.Model;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Scripts
{
	public class Character : MonoBehaviour
	{
		[SerializeField] private AudioSource audioSource;
		[SerializeField] private AudioClip shotClip;
		[SerializeField] private CameraController cameraController;
		[SerializeField] private CharacterController characterController;
		[SerializeField] private float speed;
		[SerializeField] private float gravity = -9.8f;
		[SerializeField] private LayerMask groundLayer;

		[SerializeField] private GameObject bulletFire;
		[SerializeField] private Transform bulletSpawnPoint;


		[SerializeField] private float fireRate = 50f;

		[SerializeField] private GameObject bulletHolePrefab;
		[SerializeField] private GameObject bloodPrefab;

		private float lastFireTime = 0f;
		
		private float velocity = 0f;
		private float bulletDelay;


		[SerializeField] private float groundCheckRadius = 0.2f;
		private void Start()
		{
			bulletDelay = 1f / fireRate;
		}

		[SerializeField] private SoundManager soundManager;


		private void Update()
		{
			if (Physics.CheckSphere(transform.position, groundCheckRadius, groundLayer))
			{
				velocity = 0f;
			}
			var horizontal = Input.GetAxis("Horizontal");
			var vertical = Input.GetAxis("Vertical");
			var moveDir = horizontal * transform.right + vertical * transform.forward;
			characterController.Move(speed * Time.deltaTime * moveDir.normalized);
			velocity += gravity * Time.deltaTime;
			if (Input.GetButtonDown("Jump"))
			{
				velocity = 0.05f;
			}
			characterController.Move(velocity * Vector3.up);
			
			

			lastFireTime += Time.deltaTime;
			if (Input.GetButton("Fire1"))
			{
				if (lastFireTime < bulletDelay) return;
				
				SoundManager.Instance.Play(SoundType.Fire);
				lastFireTime = 0f;
				
				var pos = Camera.main.transform.position;
				var dir = Camera.main.transform.forward;
				RaycastHit hitInfo;
				Physics.Raycast(pos, dir, out hitInfo);
				var fire = Instantiate(bulletFire, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
				fire.transform.SetParent(bulletSpawnPoint);

				if (hitInfo.collider != null)
				{
					if (hitInfo.collider.CompareTag("Target"))
					{
						var blood = Instantiate(bloodPrefab, hitInfo.point + (hitInfo.normal * 0.001f),
						                           Quaternion.LookRotation(hitInfo.normal));
						var enemy = hitInfo.collider.gameObject.GetComponent<Enemy>();
						enemy.ReceiveDamage(20);
					}
					else
					{
						var newDecal = Instantiate(bulletHolePrefab, hitInfo.point + (hitInfo.normal * 0.001f),
						                           Quaternion.LookRotation(-hitInfo.normal));
						newDecal.transform.localScale = 0.2f * Vector3.one;
					}
				}
			}
		}

		private void OnDrawGizmos()
		{
			Gizmos.DrawWireSphere(transform.position, groundCheckRadius);
		}
	}
}
