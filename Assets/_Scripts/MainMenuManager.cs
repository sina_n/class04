﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts
{
	public class MainMenuManager : MonoBehaviour
	{
		[SerializeField] private GameObject loadingPanel;
		
		public void LoadGame()
		{
			loadingPanel.SetActive(true);
			StartCoroutine(nameof(LoadCoroutine));
		}

		private IEnumerator LoadCoroutine()
		{
			var asyncOperation = SceneManager.LoadSceneAsync(1);
			while (asyncOperation.isDone)
			{
				yield return null;
			}
			loadingPanel.SetActive(false);
		}
	}
}
