﻿using System;
using UnityEngine;

namespace _Scripts.Model
{
	public enum SoundType
	{
		Fire,
		Reload,
		Walk,
	}
	
	[Serializable]
	public class SoundAsset
	{
		public SoundType key;
		public AudioClip clip;
	}
}
