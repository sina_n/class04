﻿using System.Collections.Generic;
using _Scripts.Model;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Audio;

namespace _Scripts
{
	public class SoundManager : MonoBehaviourSingleton<SoundManager>
	{
		[SerializeField] private List<SoundAsset> assets;
		[SerializeField] private AudioSource audioSource;
		[SerializeField] private AudioMixerGroup audioMixerGroup;
		public void Play(SoundType key)
		{
			var soundAsset = assets.Find(x => x.key == key);
			if (soundAsset != null)
			{
				audioSource.PlayOneShot(soundAsset.clip);
			}
		}

		public void SetVolume(float value)
		{
			audioMixerGroup.audioMixer.SetFloat("volume", Mathf.Log10(value) * 20);
		}
	}
}
